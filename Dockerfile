FROM python:3.6-alpine
RUN apk update
RUN apk add docker
RUN pip install discord.py
RUN pip install docker 

COPY . /bot

WORKDIR /bot

ENTRYPOINT ["python", "-u", "bot.py"]




